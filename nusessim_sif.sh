#!/bin/bash

function nusessim_sif_test {
    cd /setupenv
    ls
    pwd
    source setupEnv.sh -d
    echo " "
    echo " "
    echo "/cosmique_proton_generator/cosmique_proton_generator"
    /cosmique_proton_generator/cosmique_proton_generator
    echo " "
    echo " "
    source install.sh --test
    #
    #which conda
    #conda init bash
    #conda env list
    #conda deactivate
    #conda activate easchersim
}

function printHelp {
    echo " --> ERROR in input arguments"
    echo " -h     : print help"
    echo " --test : run simple test"
}

if [ $# -eq 0 ]; then
    printHelp
else
    if [ "$1" = "-h" ]; then
	printHelp
    elif [ "$1" = "--test" ]; then
	nusessim_sif_test
    else
        printHelp
    fi
fi
