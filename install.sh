#!/bin/bash

initial_dir=`dirname "$PWD"`
#echo "initial_dir $initial_dir"
instal_log=$initial_dir/'install.log'
rm -rf $instal_log
setupenvDir=$initial_dir/setupenv
terzinag4_build="terzinag4-build"

function printHelp {
    echo " --> ERROR in input arguments "
    echo " [0] -d                   : clone and install NUSES satellite full simulation"
    echo " [0] --get                : clone all modules"
    echo " [0] --build              : compile all modules"
    echo " [0] --build_terzinag4    : compile terzinag4"
    echo " [0] --test               : test all modules"
    echo " [0] --version            : get version (branch name)"
    echo " [0] --status             : get git status (branch name)"
    echo " [0] --diff               : get git diff"
    echo " [0] --pull               : get git pull"
    echo " [0] --HepRApp            : get HepRApp"
    echo " [0] --remove_all_modules : remove all modules"
    echo " [0] -h                   : print help"
}

#git@github.com:burmist-git/HepRApp.git
#https://github.com/burmist-git/HepRApp.git
#optional_module_List=(
#    HepRApp.git
#)

git_clone_https="https://gitlab.com/nuses-satellite-full-simulation/"
git_clone_ssh="git@gitlab.com:nuses-satellite-full-simulation/"
module_List=(
    'easchersim'
    'cosmique_proton_generator'
    'package_shared_library_installation_c'
    'terzinag4'
    'terzinag4_ana'
    'terzina_wfSim'
    'terzina_wfana'
    'terzina_daq'
)
nPackage=${#module_List[@]}
let nPackage=nPackage-1

### Number of threads for compilation
nthreads=`(cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l)`
#For compilation we use one less thread than total number of threads 
let nthreads=nthreads-1

function remove_all_modules {
    echo " INFO ---> remove_all_modules"
    echo " deprecated and deactivated function"
    #rm -rf ../cosmique_proton_generator
    #rm -rf ../easchersim
    #rm -rf ../package_shared_library_installation_c
    #rm -rf ../terzinag4
    #rm -rf ../terzinag4-build
    #rm -rf ../terzinag4_ana
    #rm -rf ../terzina_wfana
    #rm -rf ../terzina_wfSim
    #rm -rf ../terzina_daq
}

function printUsefulInfo {
    echo " INFO ---> printUsefulInfo"
    echo " INFO ---> initial_dir : $initial_dir "
    echo " INFO ---> instal_log  : $instal_log"
    echo " INFO ---> nthreads    : $nthreads"
}

function get_branchs {
    for i in `seq 0 $nPackage`;
    do
	cd ../${module_List[$i]}
	echo "${module_List[$i]}"
	git branch
	cd $setupenvDir
    done
}

function get_status {
    git fetch -a
    git status
    for i in `seq 0 $nPackage`;
    do
	cd ../${module_List[$i]}
	echo "${module_List[$i]}"
	git fetch -a
	git status
	cd $setupenvDir
    done
}

function get_diff {
    git diff
    for i in `seq 0 $nPackage`;
    do
	cd ../${module_List[$i]}
	echo "${module_List[$i]}"
	git diff
	cd $setupenvDir
    done
}

function get_pull {
    git pull
    for i in `seq 0 $nPackage`;
    do
	cd ../${module_List[$i]}
	echo "${module_List[$i]}"
	git pull
	cd $setupenvDir
    done
}

function get_modules {
    echo " "
    echo " "
    echo " INFO ---> get_modules"
    if [ $1 = "--https" ]; then
	git_clone_preff=$git_clone_https
    else
        git_clone_preff=$git_clone_ssh
    fi
    cd $initial_dir
    echo " INFO ---> initial_dir     : $initial_dir"
    echo " INFO ---> git_clone_preff : $git_clone_preff"
    echo " INFO ---> nPackage        : ${#module_List[@]}"
    for i in `seq 0 $nPackage`
    do
        git_rep=$git_clone_preff${module_List[$i]}'.git'
	if [ ! -d ${module_List[$i]} ]; then
	    echo " INFO ---> git clone $git_rep"
	    git clone $git_rep ${module_List[$i]}
	    cd ${module_List[$i]}
	    git checkout master
	    cd ../
	else
            echo " INFO ---> ${module_List[$i]} module already exist"
        fi
    done
    cd $initial_dir
}

function build_modules {
    echo " "
    echo " "
    echo " INFO ---> build_modules"
    cd $initial_dir
    #easchersim
    #cd $initial_dir/easchersim
    #echo " INFO ---> $initial_dir/setupenv"
    #source easchersim.sh -i
    #cd $initial_dir
    #
    #cosmique_proton_generator
    cd cosmique_proton_generator
    echo " INFO ---> cosmique_proton_generator"
    make clean; make;
    make -f Makefilecpv
    make -f Makefilemerg
    cd $initial_dir
    #
    #package_shared_library_installation_c
    cd package_shared_library_installation_c
    echo " INFO ---> package_shared_library_installation_c"
    make clean; make cleaninstall; make; make install
    cd $initial_dir
    #
    #terzina_wfSim
    cd terzina_wfSim
    echo " INFO ---> terzina_wfSim"
    make clean; make cleaninstall; make; make install
    cd $initial_dir
    #
    #terzina_wfana
    cd terzina_wfana
    echo " INFO ---> terzina_wfana"
    make clean; make cleaninstall; make; make install
    cd $initial_dir
    #
    #terzina_daq
    cd terzina_daq
    echo " INFO ---> terzina_daq"
    make clean; make cleaninstall; make; make install
    cd $initial_dir    
    #terzinag4_ana
    cd $initial_dir/setupenv
    echo " INFO ---> $initial_dir/setupenv"
    source setupEnv.sh -d
    #echo " INFO ---> LD_LIBRARY_PATH = $LD_LIBRARY_PATH"
    cd ../terzinag4_ana
    echo " INFO ---> terzinag4_ana"
    make -f Makefileterzina clean;
    make -f Makefilemgrana clean;
    make -f Makefileterzina
    #make -f Makefilemgrana
    cd $initial_dir
    build_terzinag4
    cd $initial_dir
}

function build_terzinag4 {
    mkdir -p $initial_dir/$terzinag4_build
    cd $initial_dir/$terzinag4_build
    echo " INFO ---> $terzinag4_build"
    cmake -DGeant4_DIR=$G4LIB ../terzinag4
    make -j$nthreads
    cd $initial_dir
}

function test_modules {
    echo " "
    echo " "
    echo " INFO ---> test_modules"
    #
    #cosmique_proton_generator
    cd $initial_dir
    cd cosmique_proton_generator
    echo " "
    echo " INFO ---> cosmique_proton_generator"
    echo " INFO ---> ./cosmique_proton_generator test"; ./cosmique_proton_generator
    echo " INFO ---> ./mergeDATfiles test";             ./mergeDATfiles
    echo " INFO ---> ./runcpv test";                    ./runcpv
    echo " INFO ---> ./runmerg test";                   ./runmerg
    cd $initial_dir
    #
    #package_shared_library_installation_c
    cd $initial_dir
    cd package_shared_library_installation_c
    echo " "
    echo " INFO ---> package_shared_library_installation_c"
    echo " INFO ---> ./test test"; ./test
    cd $initial_dir
    #
    #terzina_wfSim
    cd $initial_dir
    cd terzina_wfSim
    echo " "
    echo " INFO ---> terzina_wfSim"
    echo " INFO ---> ./fit_LEE test";                    ./fit_LEE
    echo " INFO ---> ./generate_waveform_template test"; ./generate_waveform_template
    echo " INFO ---> ./runterzina_wfSim test";           ./runterzina_wfSim
    cd $initial_dir
    #
    #terzina_wfana
    cd $initial_dir
    cd terzina_wfana
    echo " "
    echo " INFO ---> terzina_wfana"
    echo " INFO ---> ./runwaveform test"; ./runwaveform
    cd $initial_dir
    #
    #terzinag4_ana
    cd $initial_dir
    cd terzinag4_ana
    echo " "
    echo " INFO ---> terzinag4_ana"
    echo " INFO ---> ./runterzina test";                ./runterzina
    echo " INFO ---> ./plots_SiPM_array test";          ./plots_SiPM_array
    echo " INFO ---> ./merge_hist_files test";          ./merge_hist_files
    echo " INFO ---> ./cosmique_proton_generator test"; ./cosmique_proton_generator
    cd $initial_dir
    #
    #terzinag4-build
    cd $initial_dir
    cd terzinag4-build
    echo " "
    echo " INFO ---> terzinag4-build"
    echo " INFO ---> ./terzina test"; ./terzina
    cd $initial_dir
    #
    #easchersim
    cd $initial_dir
    cd easchersim
    echo " "
    echo " INFO ---> easchersim"
    echo " INFO ---> source easchersim.sh -testsimple"; source easchersim.sh -testsimple
    echo " INFO ---> "
    #echo " INFO ---> source conv.sh -t"; source conv.sh -t
    cd $initial_dir
    #
}

if [ $# -eq 0 ] 
then    
    printHelp
else
    if [ "$1" = "-d" ]; then
	rm -rf $instal_log
	date | tee $instal_log
	#get_modules --https | tee -a $instal_log
	get_modules --ssh | tee -a $instal_log
	build_modules | tee -a $instal_log
	test_modules | tee -a $instal_log
	printUsefulInfo | tee -a $instal_log
	echo " " | tee -a $instal_log
	echo " " | tee -a $instal_log
	date | tee -a $instal_log
    elif [ "$1" = "--version" ]; then
	get_branchs
    elif [ "$1" = "--status" ]; then
	get_status
    elif [ "$1" = "--get" ]; then
	#get_modules --http
	get_modules --ssh
    elif [ "$1" = "--diff" ]; then
	get_diff
    elif [ "$1" = "--pull" ]; then
	get_pull
    elif [ "$1" = "--build" ]; then
	build_modules
    elif [ "$1" = "--build_terzinag4" ]; then
	build_terzinag4
    elif [ "$1" = "--install" ]; then
	install_modules
    elif [ "$1" = "--test" ]; then
	test_modules
    elif [ "$1" = "--HepRApp" ]; then
	git clone git@github.com:burmist-git/HepRApp.git ../HepRApp
    elif [ "$1" = "--remove_all_modules" ]; then
	remove_all_modules
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi
