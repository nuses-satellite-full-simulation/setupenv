#!/bin/bash

function docker_list_images_container_sh {
    echo ""
    echo ""
    echo "docker images ; echo 'list docker images'"
    docker images
    echo ""
    echo ""
    echo "docker ps -a ; echo 'list docker containers'"
    docker ps -a
}

function creating_an_image_from_container {
    echo "docker commit <CONTAINER ID> nusessim" 
}

function docker_build_sh {
    N_CPU_CORES=10
    time docker build --no-cache --progress=plain --build-arg N_CPU_CORES=$N_CPU_CORES -f Dockerfile -t nusesfullsim $PWD 2>&1 | tee docker.log
    #time docker build --no-cache --progress=plain --build-arg N_CPU_CORES=$N_CPU_CORES -f Dockerfile -t nusesfullsim_miniconda $PWD 2>&1 | tee docker_miniconda.log
}

function remove_all_containers_and_images {
    docker rm $(docker ps -a | awk {'print $1'})
    docker rmi -f $(docker images | awk {'print $3'} | grep -v IMAGE | xargs)
}

function nusessim_build_sif {
    time singularity build nusessim.sif docker-daemon://nusessim:latest
}

function singularity_run_nusessim_test {
    #singularity run -B /home/burmist/home2/work/CTA/corsika_reader_my/tmp/data/:/store/cta/cta03/ nusessim.sif /run_simtelarray/run_simtelarray.sh -test_sim_telarray
    singularity run nusessim.sif ./nusessim_sif.sh --test
}

function printHelp {
    echo " --> ERROR in input arguments"
    echo " -h        : print help"
    echo " -b        : create docker image"
    echo " --dcont   : create docker from container"
    echo " --listall : list images and container"
    echo " --li      : list images"
    echo " --lc      : list containers"
    echo " --rmall   : remove all containers and images"
    echo " --sif     : create singularity (.sif) file"
    echo " --siftest : singularity - run nusessim.sif test"
}

if [ $# -eq 0 ]; then
    printHelp
else
    if [ "$1" = "-h" ]; then
	printHelp
    elif [ "$1" = "-b" ]; then
	docker_build_sh
    elif [ "$1" = "--listall" ]; then
	docker_list_images_container_sh
    elif [ "$1" = "--li" ]; then
	docker images
    elif [ "$1" = "--dcont" ]; then
	creating_an_image_from_container
    elif [ "$1" = "--lc" ]; then
	echo " " 
	echo "active" 
	docker ps
	echo " " 
	echo "all"
	docker ps -a
    elif [ "$1" = "--rmall" ]; then
	remove_all_containers_and_images
    elif [ "$1" = "--sif" ]; then
	nusessim_build_sif
    elif [ "$1" = "--siftest" ]; then
	singularity_run_nusessim_test
    else
        printHelp
    fi
fi
