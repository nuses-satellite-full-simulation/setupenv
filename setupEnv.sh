#!/bin/sh

#source $ROOTCERN_SH_FILE
#source $GEANT4MAKE_SH_FILE

package_shared_library="../package_shared_library_installation_c/install.dev/"
terzina_wfSim_library="../terzina_wfSim/install.dev/"
terzina_wfana_library="../terzina_wfana/install.dev/"
terzina_daq_library="../terzina_daq/install.dev/"
cosmique_proton_generator_library="../cosmique_proton_generator/install.dev/"

function printHelp {
    echo " --> ERROR in input arguments"
    echo " [0] -d  : setup environment"
    echo " [0] -c  : setup environment only for cosmique_proton_generator"
    echo " [0] -h  : print help"
}

if [ $# -eq 0 ];
then    
    printHelp
else
    if [ "$1" = "-d" ]; then
	#
	if [ -z "${LD_LIBRARY_PATH-}" ] ; then
	    export LD_LIBRARY_PATH="`cd $package_shared_library > /dev/null ; pwd`"
	else
	    export LD_LIBRARY_PATH="`cd $package_shared_library > /dev/null ; pwd`":${LD_LIBRARY_PATH}
	fi
	#
	export LD_LIBRARY_PATH="`cd $terzina_wfSim_library > /dev/null ; pwd`":${LD_LIBRARY_PATH}
	export LD_LIBRARY_PATH="`cd $terzina_wfana_library > /dev/null ; pwd`":${LD_LIBRARY_PATH}
	export LD_LIBRARY_PATH="`cd $terzina_daq_library > /dev/null ; pwd`":${LD_LIBRARY_PATH}
	#
	echo " INFO ---> setupEnv.sh ${@}"
	echo " INFO ---> LD_LIBRARY_PATH = $LD_LIBRARY_PATH"
	#
    elif [ "$1" = "-c" ]; then
	if [ -z "${LD_LIBRARY_PATH-}" ] ; then
	    export LD_LIBRARY_PATH="`cd $cosmique_proton_generator_library > /dev/null ; pwd`"
	else
	    export LD_LIBRARY_PATH="`cd $cosmique_proton_generator_library > /dev/null ; pwd`":${LD_LIBRARY_PATH}
	fi
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi
