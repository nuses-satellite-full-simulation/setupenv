Requirements to compile and create docker and singularity containers:

System : Ubuntu 22.04.3 LTS
Geant4 : Geant4-11.1.3
ROOT   : Release 6.28/12 - 2024-01-30

#How to use singularity

*Step 1: generate SSH key

You need to generate an ed25519 SSH key by using the command "ssh-keygen -t ed25519". When doing so it is important to use the default filename for the SSH key as on ygdrasil the ssh agent has a hard time finding keys with non-default names.

*Step 2: add SSH key to your gitlab account

Your ssh key will be in the file ~/.ssh/id_ed25519.pub. Copy the content of this file and log into gitlab. Go to https://gitlab.com/-/profile/preferences and select "SSH keys" on the sidebar on the left. Select "Add new key" and paste the content of the file into the field "key". Set the other fields as you like and click "Add key".

*Step 3: Set up SSH agent
write this in .bashrc and source after

#!/bin/bash
eval `ssh-agent -s`
ssh-add

*Step 4: Build .sif image 


singularity build --build-arg SSH_AUTH_SOCK_USER=$SSH_AUTH_SOCK Terzina_chain.sif Terzina_chain.def



Previous building with old def file

Now set up the SSH agent by pasting the following section into ~/.bashrc.

```bash

SSH_ENV=$HOME/.ssh/environment

function start_agent {
     echo "Initialising new SSH agent..."
     /usr/bin/ssh-agent | sed 's/^echo/#echo/' > ${SSH_ENV}
     echo succeeded
     chmod 600 ${SSH_ENV}
     . ${SSH_ENV} > /dev/null
     /usr/bin/ssh-add;
}

if [ -f "${SSH_ENV}" ]; then
     . ${SSH_ENV} > /dev/null
     #ps ${SSH_AGENT_PID} doesn't work under cywgin
     ps -efp ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
         start_agent;
     }
else
     start_agent;
fi

```

now use "source ~/.bashrc" or log out and log in again. Now a new SSH agent should be set up and you should receive a message about it finding your SSH key.

Alternatively you can start the ssh agent using 

```bash
eval $(ssh-agent -s)
```


and add the ssh key with

```bash
ssh-add ~/.ssh/id_ed25519
```
 

*Step 4: Set '''SSH_AUTH_SOCK'''

Use the command 

'''bash
echo $SSH_AUTH_SOCK
'''
to see the AUTH SOCK of your ssh agent. paste the path you get into line 8 of Terzina_chain.def after the = sign replacing the previous value from the echo command.

*Step 5: Build .sif image 

Use the command "singularity build Terzina_chain.sif Terzina_chain.def" to generate the singularity image.

#How to run the singularity image

To run the simulation you need to run a bash script in it that sets up env variables to use root and GEANT4 that can be found in basic_bash.sh. Adapt the script to your needs.

Next you need to bind relevant folders to specific points in the image where the simulation expect config and data files. Use the command:

"singularity run --bind  $CONFIGFOLDER:/mnt,$CONFIGFOLDER/config_v.00.00.02_sing:/config_v.00.00.02 basic_bash.sh"

with
"
$CONFIGFOLDER=/srv/beegfs/scratch/shares/trimarel/Terzina_fullchain
"

