#!/bin/bash

configgitrepo_ssh="git@gitlab.com:nuses-satellite-full-simulation/config_v.00.00.02.git"
configgitrepo_http="https://gitlab.com/nuses-satellite-full-simulation/config_v.00.00.02.git"
configDir="../config_v.00.00.02/"
geant4_crosssections_data_Dir="../"
mkdir -p $configDir

function printHelp {
    echo " --> ERROR in input arguments "
    echo " [0] -d                     : "
    echo " [0] --config               : config from repo."
    echo " [0] --CAD                  : CAD"
    echo " [0] --geant4_crosssections : geant4_crosssections"
    echo " [0] -h                     : print help"
}

function get_CAD {
    scp burmistr@login1.yggdrasil.hpc.unige.ch:/srv/verso/projects/sst1m/terzina_stl/config_CAD_v.00.00.02/*.stl $configDir/.
    #cp -r /srv/beegfs/scratch/shares/trimarel/config_CAD_v.00.00.02/*.stl $configDir/.
}

function get_geant4_crosssections_data {
    cp -r /srv/beegfs/scratch/shares/trimarel/geant4-v11.2.1_crosssection_data $geant4_crosssections_data_Dir/.
}

function get_config {
    if [ $1 = "--https" ]; then
	configgitrepo=$configgitrepo_http
    else
        configgitrepo=$configgitrepo_ssh
    fi
    git clone $configgitrepo $configDir
}
    
if [ $# -eq 0 ]; then    
    printHelp
else
    if [ "$1" = "-d" ]; then
	#get_config --https
	get_config
	get_CAD	
	get_geant4_crosssections_data
    elif [ "$1" = "--config" ]; then
	#get_config --https
	get_config
    elif [ "$1" = "--CAD" ]; then
	get_CAD
    elif [ "$1" = "--geant4_crosssections" ]; then
	get_geant4_crosssections_data
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi
