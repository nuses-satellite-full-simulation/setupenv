FROM ubuntu:20.04

ARG N_CPU_CORES=12

RUN echo "N_CPU_CORES ${N_CPU_CORES}"

#RUN apt-get update && \	
#    apt-get install -y curl git unzip

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get upgrade && \
    apt-get install -y curl git patch make bc wget apt-utils emacs htop dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev gfortran libssl-dev libpcre3-dev libglu1-mesa-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev python python-dev python3 python3-dev libxml2-dev libkrb5-dev libgsl0-dev libcfitsio-dev

RUN git clone https://gitlab.com/nuses-satellite-full-simulation/setupenv.git

##Install geant4
ARG g4rootDirName=geant4.10.04.p03
ARG g4homeDir=/$g4rootDirName
ARG duildDir=$g4homeDir/$g4rootDirName-build
ARG installDir=$g4homeDir/$g4rootDirName-install
ARG installDataDir=$installDir/share/Geant4-10.4.3/data/
ARG geant4make_sh_dir=$installDir/share/Geant4-10.4.3/geant4make/geant4make.sh
ENV GEANT4MAKE_SH_FILE=$geant4make_sh_dir
#
RUN mkdir -p $g4homeDir && \
    mkdir -p $duildDir && \
    mkdir -p $installDir
#
RUN cd $g4homeDir && \
    wget https://ftp.uni-erlangen.de/macports/distfiles/geant4/$g4rootDirName.tar.gz && \
    tar -zxvf $g4rootDirName.tar.gz && \
    cd $duildDir && \
    cmake -DGEANT4_INSTALL_DATA=ON -DCMAKE_INSTALL_PREFIX=$installDir $g4homeDir/$g4rootDirName && \
    make -j$N_CPU_CORES && \
    make install
#
#RUN mkdir -p $installDataDir && \
#    cd $installDataDir && \
#    wget https://cern.ch/geant4-data/datasets/G4NDL.4.5.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4EMLOW.7.3.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4PhotonEvaporation.5.2.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4RadioactiveDecay.5.2.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4NEUTRONXS.1.4.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4PII.1.3.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4RealSurface.2.1.1.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4SAIDDATA.1.1.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4ABLA.3.1.tar.gz && \
#    wget https://cern.ch/geant4-data/datasets/G4ENSDFSTATE.2.2.tar.gz
#G4NDL4.5
#G4EMLOW7.3
#PhotonEvaporation5.2
#RadioactiveDecay5.2
#G4NEUTRONXS1.4
#G4PII1.3
#RealSurface2.1.1
#G4SAIDDATA1.1
#G4ABLA3.1
#G4ENSDFSTATE2.2
#RUN cd $installDataDir && \
#    tar -zxvf G4NDL.4.5.tar.gz && \
#    tar -zxvf G4EMLOW.7.3.tar.gz && \
#    tar -zxvf G4PhotonEvaporation.5.2.tar.gz && \
#    tar -zxvf G4RadioactiveDecay.5.2.tar.gz && \
#    tar -zxvf G4NEUTRONXS.1.4.tar.gz && \
#    tar -zxvf G4PII.1.3.tar.gz && \
#    tar -zxvf G4RealSurface.2.1.1.tar.gz && \
#    tar -zxvf G4SAIDDATA.1.1.tar.gz && \
#    tar -zxvf G4ABLA.3.1.tar.gz && \
#    tar -zxvf G4ENSDFSTATE.2.2.tar.gz

##Install root
ARG version_root=6.18.04
ARG roothomeDirPreff=root-$version_root
ARG roothomeDir=/$roothomeDirPreff
ARG duildDir=$roothomeDir/$roothomeDirPreff-build
ARG installDir=$roothomeDir/$roothomeDirPreff-install
ARG patch_file=davix_download_patch_6.18.04.patch
ARG patch_file_full=/setupenv/$patch_file
ENV ROOTCERN_SH_FILE=$installDir/bin/thisroot.sh
RUN mkdir -p $roothomeDir && \
    mkdir -p $duildDir && \
    mkdir -p $installDir
#
RUN cd $roothomeDir && \
    wget https://root.cern.ch/download/root_v6.18.04.source.tar.gz && \
    tar -zxvf root_v6.18.04.source.tar.gz
#patch
RUN cd $roothomeDir/$roothomeDirPreff/builtins/davix/ && \
    cp $patch_file_full . && \
    patch -u < $patch_file
#
RUN cd $duildDir && \
    cmake -DCMAKE_INSTALL_PREFIX=$installDir $roothomeDir/$roothomeDirPreff && \
    make -j$N_CPU_CORES && \
    make install
#old config.
#cmake -Dclad=OFF -DCMAKE_INSTALL_PREFIX=$installDir $roothomeDir/$roothomeDirPreff

##Install miniconda
RUN curl -LO http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN bash Miniconda3-latest-Linux-x86_64.sh -p /miniconda -b
RUN rm Miniconda3-latest-Linux-x86_64.sh
ENV PATH=/miniconda/bin:${PATH}
RUN conda update -y conda
#
RUN cd /setupenv && \
    bash install.sh --get
#
RUN cd /easchersim && \
    bash easchersim.sh -i && \
    conda init bash && \
    conda config --set auto_activate_base false
