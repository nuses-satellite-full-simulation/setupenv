#!/bin/sh

package_shared_library="/home/users/b/burmistr/terzina_photon_propagation/package_shared_library_installation_c/install.00.00.01/"
terzina_wfSim_library="/home/users/b/burmistr/terzina_photon_propagation/terzina_wfSim/install.00.00.01/"
terzina_wfana_library="/home/users/b/burmistr/terzina_photon_propagation/terzina_wfana/install.00.00.01/"

function printHelp {
    echo " --> ERROR in input arguments "
    echo " [0] -d            : setup environment"
    echo " [0] -build        : build terzinag4"
    echo " [0] -p            : purge modules"
    echo " [0] -list         : list modules"
    echo " [0] -spider       : modules spider (show all the modules)"
    echo " [0] -LD_LIBRARY   : export LD_LIBRARY_PATH"
    echo " [0] -ROOT_6.24.06 : load root 6.24.06"
    echo " [0] -h            : print help"
}

if [ $# -eq 0 ] 
then    
    printHelp
else
    if [ "$1" = "-d" ]; then
	#
	module load GCC/7.3.0-2.30 GCCcore/7.3.0 OpenMPI/3.1.1 ROOT/6.14.06-Python-2.7.15 Geant4/10.5 CMake/3.11.4
	#
	geant4make_root_tmp_my="/home/users/b/burmistr/terzina_photon_propagation/G4data/"
	export G4NEUTRONHPDATA="`cd $geant4make_root_tmp_my/G4NDL4.5 > /dev/null ; pwd`"
	export G4LEDATA="`cd $geant4make_root_tmp_my/G4EMLOW7.7 > /dev/null ; pwd`"
	export G4LEVELGAMMADATA="`cd $geant4make_root_tmp_my/PhotonEvaporation5.3 > /dev/null ; pwd`"
	export G4RADIOACTIVEDATA="`cd $geant4make_root_tmp_my/RadioactiveDecay5.3 > /dev/null ; pwd`"
	export G4PARTICLEXSDATA="`cd $geant4make_root_tmp_my/G4PARTICLEXS1.1 > /dev/null ; pwd`"
	export G4PIIDATA="`cd $geant4make_root_tmp_my/G4PII1.3 > /dev/null ; pwd`"
	export G4REALSURFACEDATA="`cd $geant4make_root_tmp_my/RealSurface2.1.1 > /dev/null ; pwd`"
	export G4SAIDXSDATA="`cd $geant4make_root_tmp_my/G4SAIDDATA2.0 > /dev/null ; pwd`"
	export G4ABLADATA="`cd $geant4make_root_tmp_my/G4ABLA3.1 > /dev/null ; pwd`"
	export G4INCLDATA="`cd $geant4make_root_tmp_my/G4INCL1.0 > /dev/null ; pwd`"
	export G4ENSDFSTATEDATA="`cd $geant4make_root_tmp_my/G4ENSDFSTATE2.2 > /dev/null ; pwd`"
	#
    elif [ "$1" = "-ROOT_6.24.06" ]; then
	module load GCC/11.2.0  OpenMPI/4.1.1 ROOT/6.24.06
    elif [ "$1" = "-build" ]; then
	#ls /opt/ebsofts/MPI/GCC/7.3.0-2.30/OpenMPI/3.1.1/Geant4/10.5/share/Geant4-10.5.0/geant4make/geant4make.sh
	cmake -DGeant4_DIR=$G4INSTALL/lib64/Geant4-10.5.0 /home/users/b/burmistr/terzina_photon_propagation/terzinag4/
	make -j8
    elif [ "$1" = "-p" ]; then
	module purge
    elif [ "$1" = "-list" ]; then
	module list
    elif [ "$1" = "-spider" ]; then
	module spider
    elif [ "$1" = "-LD_LIBRARY" ]; then
	echo "LD_LIBRARY_PATH=$package_shared_library:$terzina_wfSim_library:$terzina_wfana_library:${LD_LIBRARY_PATH}"
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi

#espeak "I have done"
