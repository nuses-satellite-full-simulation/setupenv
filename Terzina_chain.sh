#!/bin/bash

in_def_file="Terzina_chain.def"
out_sif_file="Terzina_chain.sif"
out_log_file="Terzina_chain.log"
geant4_crosssection_dir="../geant4-v11.2.1_crosssection_data/"

function printHelp {
    echo " --> ERROR in input arguments "
    echo " [0] -d       : build apptainer (singularity) no modules need to be loaded"
    echo " [0] -t       : test sif file"
    echo " [0] --prodID : test sif with (head -n 1 /config_v.00.00.02/prodID)"
    echo " [0] --tg4    : test sif file (terzinag4)"
    echo " [0] -h       : print help"
}

if [ $# -eq 0 ] 
then    
    printHelp
else
    if [ "$1" = "-d" ]; then
	#
	echo " "
	echo " "
	echo " "
	date
	#
	singularity --version
	#
	rm -rf $out_sif_file
	rm -rf $out_log_file
	time singularity build --build-arg SSH_AUTH_SOCK_USER=$SSH_AUTH_SOCK $out_sif_file $in_def_file | tee -a $out_log_file
	#
	du -hs $out_sif_file
	#
	date
	#
    elif [ "$1" = "-t" ]; then
	singularity run $out_sif_file ls
	singularity run $out_sif_file ls /terzinag4-build/
	singularity run $out_sif_file pwd
    elif [ "$1" = "--prodID" ]; then
	prodID=$(head -n 1 ../config_v.00.00.02/prodID)
	echo "prodID = $prodID"
	singularity run -B ../config_v.00.00.02/:/config_v.00.00.02 $out_sif_file head -n 1 /config_v.00.00.02/prodID
	echo " "
    elif [ "$1" = "--tg4" ]; then
	#singularity run $out_sif_file ls /terzinag4-build/
	#singularity run $out_sif_file pwd
	#echo "easchersim"
	#singularity run $out_sif_file easchersim --help
	#echo "easchersim"
	#singularity run $out_sif_file echo $PATH
        inRootFile="EASCherSim_01225062.ini.npz.root"
        inTrkInfoFile="trkInfo_01225062.dat"
	#
        trkTheta=$(cat ../terzinag4/$inTrkInfoFile | awk '{print $2}' | grep -v theta)
        trkPhi=$(cat ../terzinag4/$inTrkInfoFile | awk '{print $3}' | grep -v phi)
        trkThetaDeg=$(echo "180.0/(4*a(1))*$trkTheta" | bc -l)
        trkPhiDeg=$(echo "180.0/(4*a(1))*$trkPhi" | bc -l)
        trkDistToTerzina=$(cat ../terzinag4/$inTrkInfoFile | awk '{print $11}' | grep -v distToTerzina)
        #
        echo "trkTheta         $trkTheta"
        echo "trkPhi           $trkPhi"
        echo "trkThetaDeg      $trkThetaDeg"
        echo "trkPhiDeg        $trkPhiDeg"
        echo "trkDistToTerzina $trkDistToTerzina"
	#
	singularity run -B ../config_v.00.00.02/:/config_v.00.00.02 -B $geant4_crosssection_dir:/geant4-v11.2.1-install/share/Geant4/data -B $PWD:/scratch/ $out_sif_file bash basic_bash.sh
	#
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi
